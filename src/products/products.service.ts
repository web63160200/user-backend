import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

const product: Product[] = [
  { id: 1, name: 'Lay', price: 15 },
  { id: 2, name: 'Calbee', price: 20 },
  { id: 3, name: 'SnackJack', price: 10 },
];
let lastProdId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProdId++,
      ...createProductDto,
    };
    product.push(newProduct);
    return newProduct;
  }

  findAll() {
    return product;
  }

  findOne(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return product[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...product[index],
      ...updateProductDto,
    };
    product[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = product[index];
    product.splice(index, 1);
    return deletedProduct;
  }
  reset() {
    [
      { id: 1, name: 'Lay', price: 15 },
      { id: 2, name: 'Calbee', price: 20 },
      { id: 3, name: 'SnackJack', price: 10 },
    ];
    lastProdId = 4;
    return 'Reset';
  }
}
